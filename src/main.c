#include <stdio.h>
#include <Elementary.h>
#include "../exclaim_config.h"
#include "etc.h"
#include "status.h"

Evas_Object* sideview, *sideview_root, *sideview_content;
Ecore_Animator* sb_anime = NULL;
long sideview_show = 0;

void
explode_win_enable(Evas_Object* win);

static Eina_Bool
sideview_toggle_anime_cb(void *data EINA_UNUSED, double pos)
{
	long should_show = (long)data;
	double v = ecore_animator_pos_map(pos, ECORE_POS_MAP_SINUSOIDAL, 0.0, 0.0);
	
	if (!should_show)
		v = 1.0 - v;
	
	if ((v * 80.0) >= 68) sb_anime = NULL;
	const float size = should_show ? 400.0 : 400.0;
	evas_object_move(sideview_root, (v * size) - (int)size + 68, 0);
	
	//if (pos >= 1.0) sb_anime = NULL;
	return EINA_TRUE;
}

void
toggle_sideview()
{
	sideview_show = !sideview_show;
	sb_anime = ecore_animator_timeline_add(0.4, sideview_toggle_anime_cb, (void*)(long)sideview_show);
}

static void
win_move(void* data EINA_UNUSED, Evas* e EINA_UNUSED, Evas_Object* obj, void* ev_info EINA_UNUSED)
{
	Evas_Coord w, h;
	evas_object_geometry_get(obj, NULL, NULL, &w, &h);
	evas_object_resize(sideview_root, 300, h);
}

Evas_Object*
create_sideview(Evas_Object* parent)
{
	sideview_root = E_SHOW(elm_frame_add(parent));
	E_BEGIN(sideview_root);
		elm_widget_style_set(sideview_root, "outline");
		sideview = E_SHOW(elm_scroller_add(parent));
		E_BEGIN(sideview);
			//sideview_content = E_SHOW(elm_box_add(sideview_root));
			E_BEGIN(sideview_content);
				//elm_object_content_set(sideview, sideview_content);
			E_END(sideview_content);
		E_END(sideview);
		elm_object_content_set(sideview_root, sideview);
	E_END(sideview_root);
	
	evas_object_resize(sideview_root, 300, 0);
	evas_object_move(sideview_root, 68, 0);
	
	return sideview_root;
}

static void
_cb_toggle_sideview(void* data,
                    Evas_Object* obj EINA_UNUSED,
                    void* ev_info EINA_UNUSED)
{
	toggle_sideview();
}

Evas_Object*
create_sidebar(Evas_Object* parent)
{
	Evas_Object* o;
	Evas_Object* sb = elm_toolbar_add(parent);
	evas_object_size_hint_weight_set(sb, 0.0, EVAS_HINT_EXPAND);
	evas_object_size_hint_align_set(sb, EVAS_HINT_FILL, EVAS_HINT_FILL);
	elm_toolbar_horizontal_set(sb, EINA_FALSE);
	elm_toolbar_homogeneous_set(sb, EINA_TRUE);
	elm_toolbar_shrink_mode_set(sb, ELM_TOOLBAR_SHRINK_MENU);
	
	o = elm_toolbar_item_append(sb, "home", "Home", NULL, NULL);
	elm_toolbar_item_priority_set(o, 1);
	o = elm_toolbar_item_append(sb, "user-away", "Local", NULL, NULL);
	elm_toolbar_item_priority_set(o, 0);
	o = elm_toolbar_item_append(sb, "applications-internet", "Federated", NULL, NULL);
	elm_toolbar_item_priority_set(o, 0);
	o = elm_toolbar_item_append(sb, "mail-unread", "Notifications", _cb_toggle_sideview, NULL);
	elm_toolbar_item_priority_set(o, 1);
	o = elm_toolbar_item_append(sb, "view-list-compact", "Lists", _cb_toggle_sideview, NULL);
	elm_toolbar_item_priority_set(o, -1);
	o = elm_toolbar_item_append(sb, "mail-send", "Direct", NULL, NULL);
	elm_toolbar_item_priority_set(o, -2);
	
	return sb;
}

EAPI_MAIN int
elm_main(int argc, char** argv)
{
	elm_policy_set(ELM_POLICY_QUIT, ELM_POLICY_QUIT_LAST_WINDOW_CLOSED);
	elm_app_compile_bin_dir_set(PACKAGE_BIN_DIR);
	elm_app_compile_lib_dir_set(PACKAGE_LIB_DIR);
	elm_app_compile_data_dir_set(PACKAGE_DATA_DIR);
	elm_app_info_set(elm_main, "exclaim", "checkme");
   
	Evas_Object* win = E_SHOW(elm_win_util_standard_add("exclaim_root", "Exclaim"));
	E_BEGIN(win);
	elm_win_icon_name_set(win, "email-unread");
	elm_win_autodel_set(win, EINA_TRUE);
	evas_object_event_callback_add(win, EVAS_CALLBACK_RESIZE, win_move, NULL);
	
	explode_win_enable(win);
	
	Evas_Object* root = E_SHOW(elm_box_add(win));
	E_BEGIN(root);
		elm_box_horizontal_set(root, EINA_TRUE);
		evas_object_fullsize(root);
		
		Evas_Object* sidebar = E_SHOW(create_sidebar(win));
		elm_box_pack_end(root, sidebar);

		Evas_Object* status_scr = E_SHOW(elm_scroller_add(win));
		E_BEGIN(status_scr);
			evas_object_fullsize(status_scr);

			Evas_Object* status_content = E_SHOW(elm_box_add(status_scr));
			E_BEGIN(status_content);
				evas_object_fullsize(status_content);
				elm_object_content_set(status_scr, status_content);
			
				for (int i = 0; i < 30; ++i)
				{
					struct exc_status st_test = {
						.title = "wow",
					};
					Evas_Object* status = E_SHOW(exc_status_add(status_content, st_test));
				
					elm_box_pack_end(status_content, status);
				}
				Evas_Object* lol = E_SHOW(elm_label_add(status_content));
				elm_object_text_set(lol, "¯\\_(ツ)_/¯");
				elm_box_pack_end(status_content, lol);
			E_END(status_content);

			elm_box_pack_end(root, status_scr);
		E_END(status_scr);
		
		evas_object_resize(win, ELM_SCALE_SIZE(700), ELM_SCALE_SIZE(300));
		elm_win_resize_object_add(win, root);
	E_END(root);
	E_END(win);
	
	//???
	evas_object_stack_above(sidebar, create_sideview(win));
	
	elm_run();
	return EXIT_SUCCESS;
}
ELM_MAIN()
