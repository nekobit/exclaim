#include <Elementary.h>
#include "status.h"
#include "etc.h"

#define PFP_ICON_SIZE 48
#define INTERACT_ICON_SIZE 18

/* So... due to a some strange issue, elm_hover is noticably slowing down
 * the event loop (or just in general), and I can't exactly pinpoint why.
 *
 * As a workaround, I'm using the "original" approach for how I implemented the
 *  the menus. It looks fine, but not as pretty :^)
 */
#define USE_HOVER_MENU 0

#if USE_HOVER_MENU == 1
static void
hover_btn_show(void* data, Evas_Object* obj EINA_UNUSED, void* ev_info EINA_UNUSED)
{
	Evas_Object* hover = data;
	
	evas_object_show(hover);
}
#endif

Evas_Object*
exc_status_add(Evas_Object* parent, struct exc_status status)
{
	Evas_Object* o, *ico;
	Evas_Object* root = elm_table_add(parent);
	E_BEGIN(root);
		Evas_Object* root_evas = evas_object_rectangle_add(evas_object_evas_get(parent));
		E_BEGIN(root_evas);
			evas_object_size_hint_weight_set(root_evas, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
			evas_object_size_hint_min_set(root_evas, 440, 10);
			elm_table_pack(root, root_evas, 0, 0, 1, 1);
		E_END(root_evas);
	E_END(root); // Returned and packed into later
	
	Evas_Object* st_frame = E_SHOW(elm_frame_add(parent));
	E_BEGIN(st_frame);
	elm_table_pack(root, st_frame, 0, 0, 1, 1); // Pad hack
	evas_object_size_hint_padding_set(st_frame, 3, 3, 3, 1);
	evas_object_fullsize(st_frame);

	Evas_Object* st_box = E_SHOW(elm_box_add(st_frame));
	E_BEGIN(st_box);
		elm_table_pack(root, st_box, 0, 0, 1, 1); // Pad hack
		evas_object_fullsize(st_box);
		elm_box_horizontal_set(st_box, EINA_TRUE);
		
		// Add pfp icon
		Evas_Object* st_icon = E_SHOW(elm_icon_add(st_box));
		E_BEGIN(st_icon);
			evas_object_size_hint_align_set(st_icon, 0.0, 0.0);
			evas_object_size_hint_min_set(st_icon, PFP_ICON_SIZE, PFP_ICON_SIZE);
			evas_object_size_hint_max_set(st_icon, PFP_ICON_SIZE, PFP_ICON_SIZE);
			evas_object_size_hint_padding_set(st_icon, 12, 8, 3, 8);
			elm_icon_standard_set(st_icon, "user-away");
			elm_box_pack_end(st_box, st_icon);
		E_END(st_icon);
		
		Evas_Object* st_box_rt = E_SHOW(elm_box_add(st_box));
		E_BEGIN(st_box_rt);
			evas_object_fullsize(st_box_rt);
			//evas_object_size_hint_align_set(st_box_rt, 0.0, 0.0);
			
			Evas_Object* st_box_rt_info_root = E_SHOW(elm_box_add(st_box_rt));
			evas_object_fullsize(st_box_rt_info_root);
			elm_box_horizontal_set(st_box_rt_info_root, EINA_TRUE);
			
			Evas_Object* st_box_rt_info = E_SHOW(elm_box_add(st_box_rt));
			E_BEGIN(st_box_rt_info);
				evas_object_fullsize(st_box_rt_info);
				evas_object_size_hint_align_set(st_box_rt_info, 0.0, 0.0);
				elm_box_horizontal_set(st_box_rt_info, EINA_TRUE);
				evas_object_size_hint_padding_set(st_box_rt_info, 4,4,2,4);
				
				Evas_Object* st_username = E_SHOW(elm_label_add(st_box));
				E_BEGIN(st_username);
					Eina_Strbuf* userstr = eina_strbuf_new();
					eina_strbuf_append_printf(userstr, "<b>%s</b>",
					                          "Average poster");
					elm_object_text_set(st_username, eina_strbuf_string_get(userstr));
					free(eina_strbuf_release(userstr));
					
					elm_box_pack_end(st_box_rt_info, st_username);
				E_END(st_username);
				
				Evas_Object* st_acct = E_SHOW(elm_label_add(st_box));
				E_BEGIN(st_acct);
					Eina_Strbuf* acctstr = eina_strbuf_new();
					eina_strbuf_append_printf(acctstr, "@<b>%s</b>@%s",
					                          "user", "instance.com");
					elm_object_text_set(st_acct, eina_strbuf_string_get(acctstr));
					free(eina_strbuf_release(acctstr));
					evas_object_size_hint_padding_set(st_acct, 8, 0, 0, 0);
					
					elm_box_pack_end(st_box_rt_info, st_acct);
				E_END(st_acct);
				
				o = E_SHOW(elm_icon_add(st_box));
				E_BEGIN(o);
					elm_icon_standard_set(o, "computer");
					evas_object_size_hint_min_set(o, 16, 16);
					evas_object_size_hint_padding_set(o, 6, 0, 0, 0);
					elm_box_pack_end(st_box_rt_info, o);
				E_END(o);
				
				elm_box_pack_end(st_box_rt_info_root, st_box_rt_info);
			E_END(st_box_rt_info);
			
			Evas_Object* st_box_rt_info_rt = E_SHOW(elm_box_add(st_box_rt));
			E_BEGIN(st_box_rt_info_rt);
				//evas_object_fullsize(st_box_rt_info_rt);
				evas_object_size_hint_align_set(st_box_rt_info_rt, 0.95, 0.0);
				elm_box_horizontal_set(st_box_rt_info_rt, EINA_TRUE);
				evas_object_size_hint_padding_set(st_box_rt_info_rt, 4,16,2,4);
				
				o = E_SHOW(elm_label_add(st_box));
				E_BEGIN(o);
					evas_object_fullsize(o);
					elm_object_text_set(o, "3min");
					evas_object_size_hint_align_set(o, 1.0, 0.0);
					elm_box_pack_end(st_box_rt_info_rt, o);
				E_END(o);
				
				o = E_SHOW(elm_icon_add(st_box));
				E_BEGIN(o);
					elm_icon_standard_set(o, "system-lock-screen");
					evas_object_size_hint_min_set(o, 16, 16);
					evas_object_size_hint_padding_set(o, 4, 0, 0, 0);
					elm_box_pack_end(st_box_rt_info_rt, o);
				E_END(o);
				
				elm_box_pack_end(st_box_rt_info_root, st_box_rt_info_rt);
			E_END(st_box_rt_info_rt);
			elm_box_pack_end(st_box_rt, st_box_rt_info_root);
			
			Evas_Object* st_content = E_SHOW(elm_entry_add(st_box));
			E_BEGIN(st_content);
				evas_object_fullsize(st_content);

				elm_entry_editable_set(st_content, EINA_FALSE);
				elm_entry_single_line_set(st_content, EINA_FALSE);
				elm_object_text_set(st_content, "Lol, lmao. <b>LOL</> even");
				elm_box_pack_end(st_box_rt, st_content);
			E_END(st_content);
			
			Evas_Object* st_interact_root = E_SHOW(elm_box_add(st_box));
			elm_box_horizontal_set(st_interact_root, EINA_TRUE);
			evas_object_fullsize(st_interact_root);

			Evas_Object* st_interact = E_SHOW(elm_box_add(st_box));
			E_BEGIN(st_interact);
				elm_box_horizontal_set(st_interact, EINA_TRUE);
				evas_object_fullsize(st_interact);
				evas_object_size_hint_align_set(st_interact, 0, 0);

				o = E_SHOW(elm_button_add(st_interact));
				E_BEGIN(o) // Reply
					//elm_object_text_set(o, "Reply");
					ico = E_SHOW(elm_icon_add(st_interact));
					elm_icon_standard_set(ico, "object-rotate-left");
					evas_object_size_hint_min_set(ico, INTERACT_ICON_SIZE, INTERACT_ICON_SIZE);
					elm_object_content_set(o, ico);
					elm_object_style_set(o, "blank");
					//elm_object_style_set(o, "anchor");
					elm_box_pack_end(st_interact, o);
				E_END(o)

				o = E_SHOW(elm_button_add(st_interact));
				E_BEGIN(o) // Repeat
					//elm_object_text_set(o, "Repeat");
					ico = E_SHOW(elm_icon_add(st_interact));
					elm_icon_standard_set(ico, "media-playlist-repeat");
					evas_object_size_hint_min_set(ico, INTERACT_ICON_SIZE, INTERACT_ICON_SIZE);
					elm_object_content_set(o, ico);
					elm_object_style_set(o, "blank");
					//elm_object_style_set(o, "anchor");
					elm_box_pack_end(st_interact, o);
				E_END(o)
				
				o = E_SHOW(elm_button_add(st_interact));
				E_BEGIN(o) // Like
					//elm_object_text_set(o, "Like");
					ico = E_SHOW(elm_icon_add(st_interact));
					elm_icon_standard_set(ico, "starred");
					evas_object_size_hint_min_set(ico, INTERACT_ICON_SIZE, INTERACT_ICON_SIZE);
					elm_object_content_set(o, ico);
					elm_object_style_set(o, "blank");
					//elm_object_style_set(o, "anchor");
					elm_box_pack_end(st_interact, o);
				E_END(o)
				
				o = E_SHOW(elm_button_add(st_interact));
				E_BEGIN(o) // React
					//elm_object_text_set(o, "React");
					ico = E_SHOW(elm_icon_add(st_interact));
					elm_icon_standard_set(ico, "zoom-in");
					evas_object_size_hint_min_set(ico, INTERACT_ICON_SIZE - 4, INTERACT_ICON_SIZE - 4);
					elm_object_content_set(o, ico);
					elm_object_style_set(o, "blank");
					//elm_object_style_set(o, "anchor");
					elm_box_pack_end(st_interact, o);
				E_END(o)

				elm_box_pack_end(st_interact_root, st_interact);
			E_END(st_interact);

			Evas_Object* st_interact_rt = E_SHOW(elm_box_add(st_box));
			E_BEGIN(st_interact);
				elm_box_horizontal_set(st_interact_rt, EINA_TRUE);
				evas_object_fullsize(st_interact_rt);
				evas_object_size_hint_align_set(st_interact_rt, 0.90, 0);
				
#if USE_HOVER_MENU == 1
				o = E_SHOW(elm_button_add(st_interact_rt));
				E_BEGIN(o) // Reply
					Evas_Object* hover = elm_hover_add(st_interact_rt);
#else
					o = E_SHOW(elm_hoversel_add(st_interact_rt));
					elm_hoversel_hover_parent_set(o, parent);
#endif
					elm_object_style_set(o, "blank");
					
					ico = elm_icon_add(st_interact_rt);
					elm_icon_standard_set(ico, "view-list-compact");
					evas_object_size_hint_min_set(ico, INTERACT_ICON_SIZE, INTERACT_ICON_SIZE);
					elm_object_content_set(o, ico);
					elm_box_pack_end(st_interact_rt, o);
#if USE_HOVER_MENU == 1
					evas_object_smart_callback_add(o, "clicked", hover_btn_show, hover);
					
					// End button...
					// ...Start hover
					Evas_Object* p, * q, *r;
					elm_object_style_set(hover, "popout");
					elm_hover_parent_set(hover, parent);
					// CREATE
					p = elm_button_add(st_interact_rt);
					ico = elm_icon_add(st_interact_rt);
					elm_icon_standard_set(ico, "view-list-compact");
					evas_object_size_hint_min_set(ico, 16, 16);
					elm_object_content_set(p, ico);
					// END CREATE
					elm_hover_target_set(hover, o);
					elm_object_part_content_set(hover, "middle", p);
					p = elm_button_add(st_interact_rt);
					q = elm_icon_add(st_interact_rt);
					//elm_icon_standard_set(q, "mail-mark-junk");
					elm_icon_standard_set(q, "audio-volume-muted");
					evas_object_size_hint_min_set(q, 16, 16);
					elm_object_text_set(p, "Mute");
					elm_object_content_set(p, q);
					elm_object_part_content_set(hover, "bottom", p);
					
					p = elm_button_add(st_interact_rt);
					q = elm_icon_add(st_interact_rt);
					elm_icon_standard_set(q, "edit-delete");
					evas_object_size_hint_min_set(q, 16, 16);
					elm_object_content_set(p, q);
					elm_object_part_content_set(hover, "right", p);

					p = elm_button_add(st_interact_rt);
					q = elm_icon_add(st_interact_rt);
					elm_icon_standard_set(q, "user-bookmarks");
					evas_object_size_hint_min_set(q, 16, 16);
					elm_object_content_set(p, q);
					elm_object_part_content_set(hover, "left", p);
					
					p = elm_box_add(st_interact_rt);
					q = E_SHOW(elm_button_add(st_interact_rt));
					//elm_object_text_set(q, "Report");
					r = elm_icon_add(st_interact_rt);
					elm_icon_standard_set(r, "emblem-important");
					evas_object_size_hint_min_set(r, 16, 16);
					elm_object_content_set(q, r);
					elm_box_pack_end(p, q);
					q = E_SHOW(elm_button_add(st_interact_rt));
					elm_object_text_set(q, "Pin");
					elm_box_pack_end(p, q);
					q = E_SHOW(elm_button_add(st_interact_rt));
					//elm_object_text_set(q, "Copy Link");
					r = elm_icon_add(st_interact_rt);
					elm_icon_standard_set(r, "emblem-shared");
					evas_object_size_hint_min_set(r, 16, 16);
					elm_object_content_set(q, r);
					elm_box_pack_end(p, q);
					elm_object_part_content_set(hover, "top", p);
#else
					// BEGIN items
					elm_hoversel_item_add(o, "Mute Conversation", NULL, ELM_ICON_NONE, NULL, NULL);
					elm_hoversel_item_add(o, "Pin on Profile", NULL, ELM_ICON_NONE, NULL, NULL);
					elm_hoversel_item_add(o, "Bookmark", NULL, ELM_ICON_NONE, NULL, NULL);
					elm_hoversel_item_add(o, "Delete Status", NULL, ELM_ICON_NONE, NULL, NULL);
					elm_hoversel_item_add(o, "Copy link", NULL, ELM_ICON_NONE, NULL, NULL);
					elm_hoversel_item_add(o, "Report", NULL, ELM_ICON_NONE, NULL, NULL);
					// END items
#endif
				E_END(o)
				
				elm_box_pack_end(st_interact_root, st_interact_rt);
			E_END(st_interact_rt);

			elm_box_pack_end(st_box_rt, st_interact_root);
			elm_box_pack_end(st_box, st_box_rt);
		E_BEGIN(st_box_rt);

		elm_object_content_set(st_frame, st_box);
		elm_widget_style_set(st_frame, "outline");
	E_END(st_box);
	
	E_END(st_frame);
	return root;
}
