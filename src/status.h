#ifndef STATUS_H
#define STATUS_H
#include "exc_types.h"

Evas_Object*
exc_status_add(Evas_Object* parent, struct exc_status status);

#endif // STATUS_H
