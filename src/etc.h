#ifndef ETC_H
#define ETC_H
#include <Elementary.h>
#include "../exclaim_config.h"

#define _E_COMMENT(...) do { \
  switch ((uintptr_t)__VA_ARGS__) \
    (void)__VA_ARGS__; \
  } while (0);

#define E_BEGIN _E_COMMENT
#define E_END _E_COMMENT

Evas_Object*
E_SHOW(Evas_Object* obj);

void
evas_object_fullsize(Evas_Object* obj);

#endif // ETC_H
