#include "etc.h"

Evas_Object*
E_SHOW(Evas_Object* obj)
{
	evas_object_show(obj);
	return obj;
}

void
evas_object_fullsize(Evas_Object* obj)
{
	evas_object_size_hint_weight_set(obj, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	evas_object_size_hint_align_set (obj, EVAS_HINT_FILL, EVAS_HINT_FILL);
}
