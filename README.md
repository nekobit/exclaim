# Exclaim

### A simple client for Mastodon and Pleroma.

Exclaim is a Social media client for Mastodon and Pleroma. It uses an
underlying library called `mastodont-c`. mastodont-c is being updated on the
fly alongside this project and another project.

Spice up your terrible takes with a native, efficient client written in EFL.

## Building

Exclaim uses the Meson build system. For more information please see
mesonbuild.com.

```
meson . build
ninja -C build
sudo ninja -C build install
```
